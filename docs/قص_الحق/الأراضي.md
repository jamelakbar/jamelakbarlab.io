# قص الحق - الأراضي


## قص الحق 22 : الأراضي 1 : سواد العراق كسابقة لتحول العمل العسكري من عبادة إلى وظيفة ما أدى للتخلف

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2CkVXrmlPlQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

بحمد الله وتوفيقه، فهذا قص الحق 22 وهو الحلقة الأولى لتوضيح فصل الأراضي والذي يركز على حركيات دخول الأراضي لبيت المال وذلك بالنظر في سواد العراق كمثال. وبالطبع، فإن لحركية امتلاك بيت المال لبعض الأراضي بيروقراطيات قد تؤثر على حال الأراضي المغنومة ما يؤدي لزيادة أو نقصان دخل بيت المال. والذي حدث هو أن اعتمار بيت المال بالأموال أدى إلى تحول العمل العسكري إلى وظيفة بعد أن كان عبادة ما أدى إلى تسلط الحكام من خلال العسكر.


## قص الحق 23 : الأراضي 2 : تدهور حال أراضي بيت المال وتأثير ذلك في التنمية وتوازن قوى المجتمع

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/naDVfW_HLKo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

بحمد الله فهذه الحلقة الثانية لفصل الأراضي من كتاب قص الحق، وهي الحلقة 23 لتوضيح الكتاب والتي تركز على سواد العراق كسابقة لدخول الأراضي لبيت المال ما أدى إلى انكسار الخراج وتدهور حال الأرض، وفي هذه الحلقة توضيح لآراء الفقهاء حول المسألة لتقصي الحق في أموال بيت المال من خلال الأراضي
وبإمكان المشاهدات والمشاهدين المتابعة حتى الدقيقة 14 لأن الذي يليه ليس بضرورة إلا لطلاب العلم والباحثين، ولكن ليس لمن يريدون فهم مسائل قص الحق عموما


## قص الحق 24 : الأراضي 3 : حكم الأراضي المغنومة وتأثير ذلك علينا الآن من حيث تسلط الحكام والتنمية

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/jpoTugXsAhI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

بحمد الله وتوفيقه، فهذه الحلقة 24 من توضيح كتاب قص الحق، وهي الحلقة الثالثة لشرح فصل الأراضي
في هذه الحلقة استكمال للجدل الذي وقع بين الفقهاء بين القائلين بضرورة القسمة إن لم يتنازل الغانمون عن حقهم، وبين من قالوا أن الإمام مخير بين القسمة أو إيقاف الأراضي المغنومة لنوائب المسلمين. وبالطبع فإن لكل رأي تأثير مختلف على مقصوصة الحقوق ما يؤدي لرفعة الأمة أو ذلتها، لهذا فإن دراسة هذه المسألة موضوع مهم برغم أنه انتهى قبل أكثر من ألف سنة، ذلك أن تقسيم سواد العراق كغنيمة بين الغانمين من عدمه أثر في التركيبة السياسية الاقتصادية الاجتماعية للأمة المسلمة بإيجاد جيش تحت سلطة الحاكم بجعل العمل العسكري وظيفة بعد أن كان عبادة.

## قص الحق 25 : الأراضي 4 : دحض أدلة من ذهبوا لعدم قسمة الغنائم ما سيؤدي لازدهار الاقتصاد


<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/G5KgIgoD4e0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

بحمد الله وتوفيقه، فهذه الحلقة 25 من قص الحق، وهي الرابعة والأخيرة من فصل الأراضي، وفيها توضيح لأهم أربعة أدلة لمن قالوا بقسمة الأراضي المغنومة، ما يعني أن سواد العراق والأراضي من بعدها والتي فتحت عنوة هي حق للغانمين وهذا يغلق الأبواب أمام بيت المال لسحب المال من الناس ما يؤدي لازدهار الاقتصاد لأن الثروات بأيدي الناس الذين سيستثمرونها.
